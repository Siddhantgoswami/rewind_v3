import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
  AsyncStorage
} from "react-native";

export default class createRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomName: "",
      language: ""
    };
  }
  createRoom = async () => {
    const token = await AsyncStorage.getItem("@accessToken");
    console.log("token in create Rooms is  ", token);
    if (token !== null) {
      console.log("Game id is ", this.props.navigation.getParam("game_id"));
      fetch("http://134.209.149.229:3030/room", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin

        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${token}`
        },

        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify({
          language: this.state.language,
          name: this.state.roomName,
          active: false,
          game_id: this.props.navigation.getParam("game_id", 0),
          maxMembers: 4
        }) // body data type must match "Content-Type" header
      })
        .then(response => {
          response.json();
          this.props.navigation.navigate("agora", {
            channelName: this.state.roomName,
            onCancel: message => {
              this.setState({
                visible: true,
                message
              });
              console.log("[agora]: onCancel ", message);
            }
          });
          console.log("response in create room is", response);
        })
        .catch(error => {
          console.log("error in CR is ", error);
        });
    }
  };
  render() {
    return (
      <View style={styles.root}>
        <TextInput
          placeholder={"Room Name"}
          style={styles.textInput}
          onChangeText={value => {
            this.setState({
              roomName: value
            });
          }}
        />
        <TextInput
          placeholder={"Preferred Language"}
          style={styles.textInput2}
          onChangeText={value => {
            this.setState({
              language: value
            });
          }}
        />
        <View style={styles.rect}>
          <TouchableOpacity style={styles.button} onPress={this.createRoom}>
            <Text style={styles.text3}>CREATE ROOM</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  textInput: {
    top: 196.26,
    left: 31.86,
    width: 305.17,
    height: 77.82,
    color: "#121212",
    position: "absolute"
  },
  textInput2: {
    top: 280.18,
    left: 31.86,
    width: 305.17,
    height: 77.82,
    color: "#121212",
    position: "absolute"
  },
  rect: {
    top: 412.93,
    left: "4.05%",
    width: 344.56,
    height: 73.24,
    position: "absolute"
  },
  button: {
    top: 0,
    left: "0%",
    width: "100%",
    height: 73.24,
    backgroundColor: "rgba(254,227,91,1)",
    position: "absolute",
    borderRadius: 98
  },
  text3: {
    top: "25%",
    left: "19.25%",
    width: "100%",
    height: 41.2,
    color: "rgba(13,13,13,1)",
    position: "absolute",
    fontSize: 30
  }
});
