import React, { Component } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

export default class Rooms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rooms: []
    };
  }
  addUserToTeam = async (room_Number, room) => {
    console.log("room is ", room);
    const token = await AsyncStorage.getItem("@accessToken");
    if (token !== null) {
      fetch("http://134.209.149.229:3030/get-my-id", {
        method: "GET", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin

        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${token}`
        },

        referrer: "no-referrer" // no-referrer, *client
      }).then(response => {
        response
          .json()
          .then(posts => {
            console.log("List", posts);
            fetch("http://134.209.149.229:3030/participants", {
              method: "POST", // *GET, POST, PUT, DELETE, etc.
              mode: "cors", // no-cors, cors, *same-origin
              body: JSON.stringify({
                room_id: room_Number,
                user_id: posts.id
              }),
              credentials: "same-origin", // include, *same-origin, omit
              headers: {
                "Content-Type": "application/json",
                // 'Content-Type': 'application/x-www-form-urlencoded',
                Authorization: `Bearer ${token}`
              },

              referrer: "no-referrer" // no-referrer, *client
            }).then(response => {
              response
                .json()
                .then(() =>
                  this.props.navigation.navigate("home", {
                    name: room,
                    id: room_Number
                  })
                )
                .catch(err => {
                  console.log("error", err);
                });
            });
          })
          .catch(err => {
            console.log("error", err);
          });
      });
    }
  };
  fetchUser = async () => {
    try {
      const token = await AsyncStorage.getItem("@accessToken");

      if (token !== null) {
        fetch(
          `http://134.209.149.229:3030/room?game_id=${this.props.navigation.getParam(
            "id"
          )}`,
          {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin

            credentials: "same-origin", // include, *same-origin, omit
            headers: {
              "Content-Type": "application/json",
              // 'Content-Type': 'application/x-www-form-urlencoded',
              Authorization: `Bearer ${token}`
            },

            referrer: "no-referrer" // no-referrer, *client
          }
        ).then(response => {
          console.log("Rooms response is ", response);
          response
            .json()
            .then(posts => {
              console.log("List", posts);
              this.setState({ rooms: posts.data });
            })
            .catch(err => {
              console.log("error", err);
            });
        });
      }
    } catch (err) {
      console.log(err);
    }
  };
  fetchRooms = async () => {
    try {
      const token = await AsyncStorage.getItem("@accessToken");
      console.log("token in Rooms is  ", token);
      if (token !== null) {
        fetch(
          `http://134.209.149.229:3030/room?game_id=${this.props.navigation.getParam(
            "id"
          )}`,
          {
            method: "GET", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin

            credentials: "same-origin", // include, *same-origin, omit
            headers: {
              "Content-Type": "application/json",
              // 'Content-Type': 'application/x-www-form-urlencoded',
              Authorization: `Bearer ${token}`
            },

            referrer: "no-referrer" // no-referrer, *client
          }
        ).then(response => {
          console.log("Rooms response is ", response);
          response
            .json()
            .then(posts => {
              console.log("List", posts);
              this.setState({ rooms: posts.data });
            })
            .catch(err => {
              console.log("error", err);
            });
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
  componentDidMount() {
    this.fetchRooms();
  }
  render() {
    console.log("id is this.props", this.props.navigation.getParam("id"));
    return (
      <View style={styles.root}>
        <FlatList
          renderItem={({ item, separators }) => (
            <TouchableOpacity
              style={styles.rect}
              onPress={() => {
                this.addUserToTeam(item.id, item.name);
              }}
            >
              <Text style={styles.text}>{item.name}</Text>
              <View>
                <Text>{item.language}</Text>
                <Text>
                  {item.participants.length} / {item.max_members}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={() => <View style={styles.rect2} />}
          data={this.state.rooms}
          style={styles.list}
        />
        <View style={styles.rect3}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.props.navigation.navigate("createRoom", {
                game_id: this.props.navigation.getParam("id")
              });
            }}
          >
            <Text style={styles.text2}>CREATE ROOM</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rect4}>
          <TouchableOpacity style={styles.button2}>
            <Text style={styles.text3}>JOIN ROOM</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  list: {
    top: 258.82,
    left: 0,
    width: "100%",
    height: 636.28,
    position: "absolute"
  },
  rect: {
    width: "100%",
    height: 60,
    backgroundColor: "#fff",
    padding: 16,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  text: {
    color: "#121212",
    fontSize: 14
  },
  rect2: {
    width: "100%",
    height: 1,
    backgroundColor: "black"
  },
  rect3: {
    top: 49.78,
    left: "4.46%",
    width: 344.56,
    height: 73.24,
    position: "absolute"
  },
  button: {
    top: 0,
    left: "2%",
    width: "100%",
    height: 73.24,
    backgroundColor: "rgba(254,227,91,1)",
    position: "absolute",
    borderRadius: 98
  },
  text2: {
    top: "25%",
    left: "15.25%",
    width: "100%",
    height: 41.2,
    color: "rgba(13,13,13,1)",
    position: "absolute",
    fontSize: 30
  },
  rect4: {
    top: 133.7,
    left: "5.68%",
    width: 344.56,
    height: 73.24,
    position: "absolute"
  },
  button2: {
    top: 0,
    left: "0%",
    width: "100%",
    height: 73.24,
    backgroundColor: "rgba(47,32,92,1)",
    position: "absolute",
    borderRadius: 98
  },
  text3: {
    top: "29.17%",
    left: "22.35%",
    width: "100%",
    height: 41.2,
    color: "rgba(252,230,230,1)",
    position: "absolute",
    fontSize: 30
  }
});
