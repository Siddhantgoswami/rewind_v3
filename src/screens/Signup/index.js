import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  AsyncStorage
} from "react-native";
import { Center } from "@builderx/utils";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { exportDefaultSpecifier } from "@babel/types";

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      password: "",
      language: ""
    };
  }
  enterUser = () => {
    fetch("http://134.209.149.229:3030/users", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, cors, *same-origin

      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },

      referrer: "no-referrer", // no-referrer, *client
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
        language: this.state.language
      }) // body data type must match "Content-Type" header
    }).then(response => {
      response
        .json()
        .then(async posts => {
          const token = await AsyncStorage.getItem("@accessToken");
          console.log("TOKEN:__>", token);
          if (token !== null) {
            //AsyncStorage.removeItem("@accessToken");
          }
          fetch("http://134.209.149.229:3030/authentication", {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, cors, *same-origin

            credentials: "same-origin", // include, *same-origin, omit
            headers: {
              "Content-Type": "application/json"
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },

            referrer: "no-referrer", // no-referrer, *client
            body: JSON.stringify({
              email: this.state.email,
              password: this.state.password,
              strategy: "local"
            }) // body data type must match "Content-Type" header
          })
            .then(accessToken => {
              console.log(accessToken);
              let temp = JSON.parse(accessToken._bodyText);

              AsyncStorage.setItem("@accessToken", temp.accessToken);
            })
            .then(() => {
              console.log("In last then ");
              this.props.navigation.navigate("gamesList");
            });
        })
        .catch(() => {
          console.log("error");
        });
    });
  };
  validateDetails = () => {
    console.log("Details are", this.state);
    const emailRegExp = /^[a-zA-Z0-9]+@[a-zA-Z]+.[a-zA-Z]+$/;
    if (
      this.state.email === "" ||
      this.state.name === "" ||
      this.setState.password === "" ||
      this.state.language === ""
    ) {
      alert("Please Enter all the details");
    } else if (!this.state.email.match(emailRegExp)) {
      alert("invalid Email");
    } else {
      this.enterUser();
    }
  };
  render() {
    return (
      <View style={styles.root}>
        <Center horizontal>
          <Image
            source={require("../../../assets/images/logoMech.jpeg")}
            resizeMode={"contain"}
            style={styles.image}
          />
        </Center>
        <TextInput
          placeholder={"Username"}
          editable={true}
          selectionColor={"rgba(154,23,23,1)"}
          underlineColorAndroid={"rgba(12,12,12,1)"}
          style={styles.textInput}
          onChangeText={value => {
            this.setState({
              name: value
            });
          }}
        />
        <TextInput
          placeholder={"Password"}
          editable={true}
          secureTextEntry={true}
          selectionColor={"rgba(154,23,23,1)"}
          underlineColorAndroid={"rgba(18,17,17,1)"}
          style={styles.textInput2}
          onChangeText={value => {
            this.setState({
              password: value
            });
          }}
        />

        <TextInput
          placeholder={"Email"}
          editable={true}
          selectionColor={"rgba(154,23,23,1)"}
          underlineColorAndroid={"rgba(14,1,1,1)"}
          style={styles.textInput3}
          onChangeText={value => {
            this.setState({
              email: value
            });
          }}
        />
        <TextInput
          placeholder={"Preferred Language"}
          editable={true}
          selectionColor={"rgba(154,23,23,1)"}
          underlineColorAndroid={"rgba(14,1,1,1)"}
          style={styles.textInput4}
          onChangeText={value => {
            this.setState({
              language: value
            });
          }}
        />
        <View style={styles.group}>
          <TouchableOpacity
            style={styles.button2}
            onPress={this.validateDetails}
          >
            <Text style={styles.text4}>Signup</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rect} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    borderRadius: 0,
    borderColor: "#000000",
    borderWidth: 0
  },
  image: {
    top: 67.6,
    width: 200,
    height: 200,
    position: "absolute",
    borderRadius: 100
  },
  textInput: {
    top: 313.95,
    left: 31.71,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(254,227,91,1)",
    borderWidth: 0
  },
  textInput2: {
    top: 406,
    left: 31.71,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(47,32,92,1)",
    borderWidth: 0
  },
  group: {
    top: 682.16,
    left: 45.57,
    width: 297.99,
    height: 58.21,
    position: "absolute"
  },
  button2: {
    top: 0,
    left: 0,
    width: 297.99,
    height: 58.21,
    backgroundColor: "rgba(47,32,92,1)",
    position: "absolute",
    borderRadius: 30
  },
  text4: {
    top: 16.63,
    left: 112.96,
    width: 80.07,
    height: 30.49,
    color: "rgba(250,246,246,1)",
    position: "absolute",
    fontSize: 23
  },
  textInput3: {
    top: 498.05,
    left: 31.57,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(47,32,92,1)",
    borderWidth: 0
  },
  textInput4: {
    top: 590.11,
    left: 31.57,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(47,32,92,1)",
    borderWidth: 0
  },
  rect: {
    top: 46.44,
    left: -848.79,
    width: 372.84,
    height: 702.71,
    backgroundColor: "rgba(230, 230, 230,1)",
    position: "absolute"
  }
});
