import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Text
} from "react-native";
import { Center } from "@builderx/utils";
import { AsyncStorage } from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isUserValid: false
    };
  }
  validateCredentials = async () => {
    console.log("Credentials are", this.state);
    if (this.state.email === "" && this.state.password === "") {
      alert("Please fill both the fields");
    } else if (this.state.email === "") {
      alert("Please fill your email ID");
    } else if (this.state.password === "") {
      alert("Please fill your password");
    } else {
      fetch("http://134.209.149.229:3030/authentication", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin

        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json"
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },

        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
          strategy: "local"
        }) // body data type must match "Content-Type" header
      }).then(response => {
        response
          .json()
          .then(accessToken => {
            console.log("Posts:", accessToken.accessToken);
            AsyncStorage.setItem("@accessToken", accessToken.accessToken);
          })
          .then(() => this.props.navigation.navigate("gamesList"))
          .catch(err => {
            console.log("error", err);
            // onError();
            alert("Invalid User");
          });
      });
    }
  };
  handleEmailChange = value => {
    this.setState({
      name: value
    });
  };
  handlePasswordChange = value => {
    this.setState({
      password: value
    });
  };
  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={styles.root}>
          <View style={styles.group4}>
            <Image
              source={require("../../../assets/images/logoMech.jpeg")}
              resizeMode={"contain"}
              style={styles.image}
            />
            <View style={styles.group3}>
              <TextInput
                onChangeText={value => {
                  this.setState({
                    email: value
                  });
                }}
                placeholder={"Email"}
                editable={true}
                selectionColor={"rgba(154,23,23,1)"}
                underlineColorAndroid={"rgba(119,48,48,1)"}
                style={styles.textInput}
              />
              <TextInput
                placeholder={"Password"}
                editable={true}
                secureTextEntry={true}
                selectionColor={"rgba(154,23,23,1)"}
                underlineColorAndroid={"rgba(119,48,48,1)"}
                style={styles.textInput2}
                onChangeText={value => {
                  this.setState({
                    password: value
                  });
                }}
              />
            </View>
            <View style={styles.group}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.validateCredentials}
              >
                <Center>
                  <Text style={styles.text3}>Login</Text>
                </Center>
              </TouchableOpacity>
            </View>
            <View style={styles.group2}>
              <TouchableOpacity
                style={styles.button2}
                onPress={() => this.props.navigation.navigate("signup")}
              >
                <Text style={styles.text4}>Signup</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    flexDirection: "column",
    borderRadius: 0,
    borderColor: "#000000",
    borderWidth: 0
  },
  group4: {
    width: 374.22,
    height: 704.1,
    marginTop: 55
  },
  scrollArea: {
    top: 0,
    left: 0,
    width: 374.22,
    height: 704.1,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  scrollArea_contentContainerStyle: {
    width: 374.22,
    height: 704.1
  },
  image: {
    top: 18.39,
    left: "23.39%",
    width: 200,
    height: 200,
    position: "absolute",
    borderRadius: 100
  },
  group3: {
    top: 274.43,
    left: 27.72,
    width: 314.63,
    height: 138.6,
    position: "absolute"
  },
  textInput: {
    top: 0,
    left: 0,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(254,227,91,1)",
    borderWidth: 0
  },
  textInput2: {
    top: 84.55,
    left: 2.77,
    width: 311.85,
    height: 54.05,
    color: "#121212",
    position: "absolute",
    borderRadius: 19,
    borderColor: "rgba(47,32,92,1)",
    borderWidth: 0
  },
  group: {
    top: 469.86,
    left: 37.42,
    width: 297.99,
    height: 58.21,
    position: "absolute"
  },
  button: {
    top: 0,
    left: 0,
    width: 297.99,
    height: 58.21,
    backgroundColor: "rgba(254,227,91,1)",
    position: "absolute",
    borderRadius: 31,
    borderColor: "#000000",
    borderWidth: 0
  },
  text3: {
    top: 10.4,
    left: 112.96,
    width: 72.07,
    height: 35.49,
    color: "black",
    position: "absolute",
    fontSize: 23
  },
  group2: {
    top: 561.34,
    left: 38.81,
    width: 297.99,
    height: 58.21,
    position: "absolute"
  },
  button2: {
    top: 0,
    left: 0,
    width: 297.99,
    height: 58.21,
    backgroundColor: "rgba(47,32,92,1)",
    position: "absolute",
    borderRadius: 30
  },
  text4: {
    top: 16.63,
    left: 112.96,
    width: 79.07,
    height: 35.49,
    color: "rgba(250,246,246,1)",
    position: "absolute",
    fontSize: 23
  }
});
