import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList,
  AsyncStorage
} from "react-native";

export default class Untitled extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gamesList: []
    };
  }
  fetchGameList = async () => {
    console.log("Entered Fetch");
    try {
      const token = await AsyncStorage.getItem("@accessToken");
      console.log("token is ", token);
      if (token !== null) {
        fetch("http://134.209.149.229:3030/game", {
          method: "GET", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, cors, *same-origin

          credentials: "same-origin", // include, *same-origin, omit
          headers: {
            "Content-Type": "application/json",
            // 'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Bearer ${token}`
          },

          referrer: "no-referrer" // no-referrer, *client
        }).then(response => {
          response
            .json()
            .then(posts => {
              console.log("List", posts);
              this.setState({ gamesList: posts.data });
            })
            .catch(err => {
              console.log("error", err);
            });
        });
      }
    } catch (error) {
      console.log(error);
    }
  };
  componentDidMount() {
    this.fetchGameList();
  }
  render() {
    return (
      <View style={styles.root}>
        <View style={styles.rect}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.text}>GAMES</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          renderItem={({ item, separators }) => (
            <TouchableOpacity
              style={styles.rect2}
              onPress={() => {
                this.props.navigation.navigate("rooms", { id: item.id });
              }}
              id={item.id}
            >
              <Text style={styles.text2}>{item.name}</Text>
            </TouchableOpacity>
          )}
          data={this.state.gamesList}
          style={styles.list}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  rect: {
    top: 65.04,
    left: "3.65%",
    width: 344.56,
    height: 73.24,
    position: "absolute"
  },
  button: {
    top: 0,
    left: "0%",
    width: 344.56,
    height: 73.24,
    backgroundColor: "rgba(47,32,92,1)",
    position: "absolute",
    borderRadius: 98
  },
  text: {
    top: "25%",
    left: "34.74%",
    width: 134.13,
    height: 41.2,
    color: "rgba(244,240,240,1)",
    position: "absolute",
    fontSize: 30
  },
  list: {
    top: 191.68,
    left: 0,
    height: 532.52,
    position: "absolute",
    right: 0
  },
  rect2: {
    width: "100%",
    height: 48,
    backgroundColor: "#fff",
    padding: 16
  },
  text2: {
    color: "#121212",
    fontSize: 14
  }
});
