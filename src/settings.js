import { DefaultTheme } from "react-native-paper";

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#2F205C",
    accent: "#f1c40f"
  }
};

export const APPID = "d0b12a26d033474d87c22ab487a4c87d";

export const title = "Mech Mocha";
