/* @flow */
import * as React from "react";
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  PermissionsAndroid
} from "react-native";
import { Header } from "react-navigation";
import {
  TextInput,
  Button,
  withTheme,
  Appbar,
  Dialog,
  Paragraph,
  ActivityIndicator
} from "react-native-paper";
import { title } from "../settings";

async function requestCameraAndAudioPermission() {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
    ]);
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

type Props = {
  theme: Theme,
  navigation: any
};

type State = {
  text: string
};

export class Index extends React.Component<Props, State> {
  static navigationOptions = {
    header: (
      <Appbar.Header>
        <Appbar.Content title={title} />
      </Appbar.Header>
    )
  };

  state = {
    text: "",
    visible: false,
    message: null
  };

  _hideDialog = () => {
    this.setState({
      visible: false,
      message: null
    });
  };

  componentWillMount() {
    if (Platform.OS === "android") {
      requestCameraAndAudioPermission().then(_ => {});
    }
  }
  componentDidMount() {
    this.props.navigation.navigate("agora", {
      channelName: this.props.navigation.getParam("name"),
      onCancel: message => {
        this.setState(
          {
            visible: true,
            message
          },
          () => {
            this.removeParticipant();
          }
        );
        console.log("[agora]: onCancel ", message);
      }
    });
  }

  async removeParticipant() {
    console.log("Removing***");
    const token = await AsyncStorage.getItem("@accessToken");
    console.log("token in create Rooms is  ", token);
    if (token !== null) {
      console.log("Game id is ", this.props.navigation.getParam("game_id"));
      let id = this.props.navigation.getParam("id");
      fetch(`http://134.209.149.229:3030/participant/${id}`, {
        method: "DELETE", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin

        credentials: "same-origin", // include, *same-origin, omit
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${token}`
        },

        referrer: "no-referrer" // no-referrer, *client
        // body: JSON.stringify({
        //   language: this.state.language,
        //   name: this.state.roomName,
        //   active: false,
        //   game_id: this.props.navigation.getParam("game_id", 0),
        //   maxMembers: 4
        // }) // body data type must match "Content-Type" header
      })
        .then(response => {
          response.json();
          console.log(response);
          // this.props.navigation.navigate("agora", {
          //   channelName: this.state.roomName,
          //   onCancel: message => {
          //     this.setState({
          //       visible: true,
          //       message
          //     });
          //     console.log("[agora]: onCancel ", message);
          //   }
          // });
          this.props.navigation.navigate("rooms");
          console.log("response in create room is", response);
        })
        .catch(error => {
          console.log("error in CR is ", error);
        });
    }
  }

  render() {
    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    const { state, navigate } = this.props.navigation;

    return (
      // <KeyboardAvoidingView
      //   style={styles.wrapper}
      //   keyboardVerticalOffset={Platform.select({
      //     ios: 0,
      //     android: Header.HEIGHT + 64
      //   })}
      //   behavior={Platform.OS === "ios" ? "padding" : null}
      //   keyboardVerticalOffset={80}
      // >
      //   <Dialog visible={this.state.visible} onDismiss={this._hideDialog}>
      //     <Dialog.Title>Alert</Dialog.Title>
      //     <Dialog.Content>
      //       <Paragraph>{this.state.message}</Paragraph>
      //     </Dialog.Content>
      //     <Dialog.Actions>
      //       <Button onPress={this._hideDialog}>Done</Button>
      //     </Dialog.Actions>
      //   </Dialog>
      //   <ScrollView
      //     style={[styles.container, { backgroundColor: background }]}
      //     keyboardShouldPersistTaps={"always"}
      //     removeClippedSubviews={false}
      //   >
      //     <TextInput
      //       style={styles.inputContainerStyle}
      //       label="channel name"
      //       placeholder="alphabet"
      //       value={this.state.text}
      //       onChangeText={text => this.setState({ text })}
      //     />
      //     <Button
      //       style={styles.buttonContainerStyle}
      //       onPress={() =>
      //         navigate("agora", {
      //           channelName: this.state.text,
      //           onCancel: message => {
      //             this.setState({
      //               visible: true,
      //               message
      //             });
      //             console.log("[agora]: onCancel ", message);
      //           }
      //         })
      //       }
      //     >
      //       join channel
      //     </Button>
      //     <Button
      //       style={styles.buttonContainerStyle}
      //       onPress={() => navigate("login")}
      //     >
      //       login
      //     </Button>
      //     <Button
      //       style={styles.buttonContainerStyle}
      //       onPress={() => navigate("gamesList")}
      //     >
      //       gamesList
      //     </Button>
      //   </ScrollView>
      // </KeyboardAvoidingView>
      <ActivityIndicator style={styles.container} />
    );
  }
}

const styles = StyleSheet.create({
  colors: {
    backgroundColor: "#2F205C"
  },
  container: {
    backgroundColor: "#F5FCFF",
    flex: 1,
    flexDirection: "column"
  },
  wrapper: {
    flex: 1
  },
  inputContainerStyle: {
    margin: 8,
    flex: 2
  },
  buttonContainerStyle: {
    flex: 2
  },
  contenStyle: {
    textAlign: "center"
  }
});

export default withTheme(Index);
